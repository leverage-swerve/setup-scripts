#!/bin/bash

# requires omvf uefi firmware package



qemu-system-x86_64 -drive if=pflash,format=raw,unit=0,readonly,file=/usr/share/edk2-ovmf/OVMF_CODE.fd -drive if=pflash,format=raw,unit=1,file=/usr/share/edk2-ovmf/OVMF_VARS.fd $@
