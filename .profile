alias vi=nvim
export ddg="https://html.duckduckgo.com/html?q="
alias gsend="git add -A; git commit -m 'commit'; git push"

# inspired by 
# https://git.sr.ht/~bt/alpine-linux-setup/


export EDITOR=/usr/bin/vim
export BROWSER=links
#export BROWSER=qutebrowser

# Session
export XDG_SESSION_TYPE=wayland
export XDG_SESSION_DESKTOP=sway
export XDG_CURRENT_DESKTOP=sway

# GTK
export MOZ_ENABLE_WAYLAND=1
export MOZ_DBUS_REMOTE=1

# elementary 
export ECORE_EVAS_ENGINE=wayland-egl
export ELM_ENGINE=wayland_egl

# java
export _JAVA_AWT_WM_NONREPARENTING=1
export NO_AT_BRIDGE=1
export BEMENU_BACKEND=wayland

# Qt
export QT_QPA_PLATFORM=wayland



if test -z "${XDG_RUNTIME_DIR}"; then 
	export XDG_RUNTIME_DIR=/tmp/$(id -u)-runtime-dir 
	if ! test -d "${XDG_RUNTIME_DIR}"; then 
		mkdir "${XDG_RUNTIME_DIR}" 
		chmod 0700 "${XDG_RUNTIME_DIR}" 
	fi
fi


export PATH="$PATH:$HOME/bin"



#case $- in *i*)
#  fish=$(command -v fish)
#  if [ -x "$fish" ]; then
#    export SHELL="$fish"
#    exec $fish
#  fi
#esac

